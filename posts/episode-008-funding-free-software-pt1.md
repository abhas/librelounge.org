title: Episode 8: Funding Free Software Development (pt1)
date: 2019-01-18 16:17:00
tags: episode, free software
summary: Free Software, Funding
enclosure: title:mp3 url:"https://archive.org/download/librelounge-ep-008/librelounge-ep-008.mp3" length:"25038046" duration:"01:14:42"
enclosure: title:ogg url:"https://archive.org/download/librelounge-ep-008/librelounge-ep-008.ogg" length:"27344867" duration:"01:14:42"
---
In this first part of a two parter, Serge and Chris talk about funding models for Free Software development.

Listen to Part 2 [here](https://librelounge.org/episodes/episode-9-funding-free-software-development-pt2.html).

