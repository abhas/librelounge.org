title: Episode 5: Karen Sandler and Software Freedom Conservancy
date: 2018-12-28 16:40:00
tags: episode, faif, conservancy, outreachy
summary: Karen Sandler and Software Freedom Conservancy
enclosure: title:mp3 url:"https://archive.org/download/librelounge-ep-005/librelounge-ep-005.mp3"
---
In their first interview, Chris and Serge interview Karen Sandler, Executive Director of Software Freedom Conservancy, Founder of Outreachy, and co-host of the Free as in Freedom podcast.

Show Links:

- [Software Freedom Conservancy (sfconservancy.org)](https://sfconservancy.org)
- [Become a Software Freedom Conservancy Supporter! (sfconservancy.org)](https://sfconservancy.org/supporter/)
- [Free as in Freedom (faif.us)](http://faif.us)
- [Git's Email from Karen (public-inbox.org)](https://public-inbox.org/git/20170202022655.2jwvudhvo4hmueaw@sigill.intra.peff.net/)
- [Gender Patch Study (livescience.com)](https://www.livescience.com/53729-bias-against-female-coders.html)
- [Dark Hands and Soap Dispenders (mic.com)](https://mic.com/articles/124899/the-reason-this-racist-soap-dispenser-doesn-t-work-on-black-skin)
- [Audio from Software Freedom with Karen Sandler and Molly de Blanc at HOPE (hope.net)](https://xii.hope.net/audio/C16_Introduction_to_User_Freedom.mp3)
- [Video from Introduction to User Freedom at Debconf (youtube)](https://www.youtube.com/watch?v=qo_WH1bYgbo)
- [The "Printer Story" (fsf)](https://www.fsf.org/blogs/community/201cthe-printer-story201d-redux-a-testimonial-about-the-injustice-of-proprietary-firmware)
- [Ledger (the accounting system used by Conservancy) (ledger-cli.org)](https://www.ledger-cli.org/)
- [Beancount (the accounting system Conservnacy is considering) (furius.ca)](http://furius.ca/beancount/)
- [Plain Text Accounting (plaintextaccounting.org)](https://plaintextaccounting.org)
- [Gandi (the domain registrar that supports Conservancy) (gandi.net)](http://gandi.net/)
