title: Episode 6: Free Software Video Games!
date: 2019-01-04 14:30:00
tags: games
summary: Free Software Games, Games and More Games
enclosure: title:mp3 url:"https://archive.org/download/librelounge-ep-006/librelounge-ep-006.mp3"
---
In this first of the year episode, Chris and Serge take a break from heady topics and talk about a few of their favorite Free Software video games.

Show links:

- [Nethack](https://www.nethack.org/)
- [New ATI Card Pushes Limits of ASCII Gaming](http://www.bbspot.com/News/2003/02/ati_ascii.html)
- [Extreme Tux Racer](https://sourceforge.net/projects/extremetuxracer/)
- [Dungeon Crawl Stone Soup](https://crawl.develz.org/)
- [Globulation 2](https://globulation2.org)
- [Pixel Dungeon](http://pixeldungeon.watabou.ru/)
- [Kobo Deluxe](http://www.olofson.net/kobodl/)
- [Frozen Bubble](http://www.frozen-bubble.org/)
- [Endless Sky](https://endless-sky.github.io/)
- [Barbie Seahorse Adventures](http://www.imitationpickles.org/barbie/)
- [Empty Epsilon](http://daid.github.io/EmptyEpsilon/)
