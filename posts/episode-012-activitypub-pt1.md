title: Episode 12: ActivityPub Part 1
date: 2019-03-08 14:45:00
tags: episode, federation, reviewpub, activitypub
summary: ActivityPub Part 1
enclosure: title:mp3 url:"https://archive.org/download/librelounge-ep-012/librelounge-ep-012.mp3" length:"51220584" duration:"00:47:42"
enclosure: title:ogg url:"https://archive.org/download/librelounge-ep-012/librelounge-ep-012.ogg" length:"23248039" duration:"00:47:42"
---

Join us for the first in an ongoing series on ActivityPub Chris and Serge dive into ActivityPub, explaining how the social networking protocol works, how it handles different  audiences, understanding the Subject-Verb-Object model of communication, as well as how following and followers work.

ReviewPub materials coming soon!

Links:

- [ActivityPub (w3c)](https://www.w3.org/TR/activitypub/)
