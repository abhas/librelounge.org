title: Episode 13: Object Capabilities with Kate Sills
date: 2019-03-15 12:45
tags: episode, fosdem, copyleftconf, lisp, free software
summary: FOSDEM, CopyleftConf and Spritely
enclosure: title:mp3 url:"https://archive.org/download/librelounge-ep-013/librelounge-ep-013.mp3" length:"40097873" duration:"00:41:18"
enclosure: title:ogg url:"https://archive.org/download/librelounge-ep-013/librelounge-ep-013.ogg" length:"22430477" duration:"00:41:17"
---

The Libre Lounge crew invite Kate Sills from Agoric to help explain and explore
Object Capabilities, an alternative to traditional ACL (Access Control List) or
authentication based mechanisms.

Links:

- [Agoric (agoric.com)](https://agoric.com)
- [POLA Would Have Prevented the Event-Stream Incident (medium.com)](https://medium.com/agoric/pola-would-have-prevented-the-event-stream-incident-45653ecbda99)
- [What are Object Capabilities? (habitatchronicles.com)](http://habitatchronicles.com/2017/05/what-are-capabilities/)
- [An OCAP Approach to Safe Javascript (docs.google.com)](https://docs.google.com/document/d/1pPiu3cjBT5OqEgqtsdDJcW5g1QsgmxvIHQjdkrPej3U/edit#heading=h.2ew6uxes5hc5)
- [The XKCD Sandbox Cycle (xkcd.com)](https://xkcd.com/2044/)
- [Professor David Wagner Gives a Google Tech Talk on Object Capabilities for Security (youtube)](https://www.youtube.com/watch?v=EGX2I31OhBE)





