title: Episode 11: What is Federation? Reviewpub.
date: 2019-02-22 15:00:00
tags: episode, federation, reviewpub, activitypub
summary: What is Federation? Reviewpub
enclosure: title:mp3 url:"https://archive.org/download/librelounge-ep-011/librelounge-ep-011.mp3" length:"36743665" duration:"00:32:15"
enclosure: title:ogg url:"https://archive.org/download/librelounge-ep-011/librelounge-ep-011.ogg" length:"16772525" duration:"00:32:15"
---

Serge and Chris talk about the concept of Fedederation, how ActivityPub fits in and Serge introduces a new project called ReviewPub to create a simple ActivityPub implementation that listeners can follow along with.
